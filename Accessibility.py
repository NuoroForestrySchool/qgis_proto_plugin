import processing
from qgis.core import QgsProject

# Get the current project
project = QgsProject.instance()
# Get the default folder for the current project
wdir = project.homePath() + "/"

# Main ARGUMENTS !!! file names to be tailored to your case
input_dem    = wdir + "DEM10_clip.tif"
streets      = wdir + "Viabilita_CLIP/Viabilita_CLIP.shp"
# Arguments with defaul values
walking_h = 4   #  Km/h  walking speed on flat (LT 10%) slope
walking_v = 400 #  m/h  walking speed in the vertical direction
# Auxiliary files
style_path   = wdir + "Accessibility.qml"
# OUTPUT file name
accessibility= wdir + "Accessibility.tif"

dbg = False

# SLOPE
slope_r = processing.run("grass7:r.slope.aspect",
{ '-a' : True, '-e' : False, '-n' : False, 
'GRASS_RASTER_FORMAT_META' : '', 'GRASS_RASTER_FORMAT_OPT' : '', 
'GRASS_REGION_CELLSIZE_PARAMETER' : 0, 'GRASS_REGION_PARAMETER' : None, 
'elevation' : input_dem, 
'format' : 1, 'precision' : 0,
'slope' : QgsProcessing.TEMPORARY_OUTPUT, 
'zscale' : 1 }
)

print("  *** tmp1.out: ", slope_r)

# compute walking time params
rasterLyr = QgsRasterLayer(slope_r['slope'], "Slope")
cellSize = rasterLyr.rasterUnitsPerPixelX(), rasterLyr.rasterUnitsPerPixelY()
pixel_dim = ((cellSize[0] + cellSize[1]) / 2 + math.sqrt(cellSize[0]**2 + cellSize[1]**2))/2
print("  *** Pixel average horizontal size: ", pixel_dim)
h_wspeed_m_m   = walking_h *1000 / 60
v_u_wspeed_m_m = walking_v / 60

# WALKING TIME
walkTime_r = processing.run("grass7:r.mapcalc.simple", {
    'a' : slope_r['slope'], 
    'expression': f"{pixel_dim} / min({h_wspeed_m_m}, ({v_u_wspeed_m_m} / (max(.1, A) / 100)))",
    'GRASS_RASTER_FORMAT_OPT': '',
    'GRASS_RASTER_FORMAT_META': '',
    'GRASS_REGION_PARAMETER': None,
    'output': QgsProcessing.TEMPORARY_OUTPUT
})
print("  *** tmp2.out: ", walkTime_r)

# Get the extent of the raster layer
rlayer = QgsRasterLayer(walkTime_r['output'])
extent = rlayer.extent()
# Extract the extent values
x_min = extent.xMinimum()
x_max = extent.xMaximum()
y_min = extent.yMinimum()
y_max = extent.yMaximum()
# Create the input dictionary for gdal.Rasterize()
#input_dict = {
#    'EXTENSION': [x_min, x_max, y_min, y_max, [{rlayer.crs().authid()}]],
#    # Add other input parameters as needed
#}

## raster of STREETS
Streets_r = processing.run("gdal:rasterize", {
'INPUT': streets,
'FIELD':'','BURN':1,'USE_Z':False,'UNITS':1,
'WIDTH': cellSize[0],'HEIGHT': cellSize[1],
'EXTENT':f"{x_min}, {x_max}, {y_min}, {y_max} [{rlayer.crs().authid()}]",
'NODATA':0,'OPTIONS':'','DATA_TYPE':5,'INIT':None,'INVERT':False,'EXTRA':'',
'OUTPUT':QgsProcessing.TEMPORARY_OUTPUT})
print("  *** tmp3.out: ", Streets_r)

# ACCESS TIME
Access_time = processing.run("grass7:r.cost", {
'input': walkTime_r['output'],
'start_coordinates':None,'stop_coordinates':None,
'-k':False,'-n':True,'start_points':None,'stop_points':None,
'start_raster': Streets_r['OUTPUT'],
'max_cost':0,'null_cost':None,'memory':300,
'output': QgsProcessing.TEMPORARY_OUTPUT,
'nearest':'','outdir':'',
'GRASS_REGION_PARAMETER':None,'GRASS_REGION_CELLSIZE_PARAMETER':0,
'GRASS_RASTER_FORMAT_OPT':'','GRASS_RASTER_FORMAT_META':'',
'GRASS_SNAP_TOLERANCE_PARAMETER':-1,'GRASS_MIN_AREA_PARAMETER':0.0001})
print("  *** tmp4.out: ", Access_time)

# ACCESSIBILITY
processing.run("grass7:r.reclass", {
'input': Access_time['output'],
'rules':'',
'txtrules':"0 thru 5 = 1 max 10 min\n5 thru 10 = 2 max 20 min\n10 thru 15 = 3 max 30 min\n15 thru 20 =  4 30-40 min\n* = 5 oltre 40 min",
'output': accessibility,
'GRASS_REGION_PARAMETER':None,'GRASS_REGION_CELLSIZE_PARAMETER':0,
'GRASS_RASTER_FORMAT_OPT':'','GRASS_RASTER_FORMAT_META':''})

if (dbg) : iface.addRasterLayer(slope_r['slope'], "Slope")
if (dbg) : iface.addRasterLayer(walkTime_r['output'], "Time to cross a pixel")
if (dbg) : iface.addRasterLayer(Access_time['output'], "Access time")

rasterlayer = iface.addRasterLayer(accessibility, "Accessibility")

rasterlayer.loadNamedStyle(style_path)
iface.layerTreeView().refreshLayerSymbology(rasterlayer.id())
rasterlayer.triggerRepaint()

print("  *** The END")