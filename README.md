# Qgis_Proto_PlugIn

## Name
Repo of SW that could become a Qgis plugin

## Description
1) Forest accessibility evaluatio

## Installation
Download the Qgis project (with all the files)   
Open Python console     Ctrl+Alt+P   
Show editor  (3rd icon under Console)
Load the py source in the py editor (1st icon of the editor)   

Tailor input (and output if desired) to suit your file names (and paths, realtive to the projects path)

## Usage
Run the py source

## Support
If necessary, use issue tracker

## Roadmap
If you like to help, I would like to make it a 'plugin'

## Authors and acknowledgment
co-authots welcome

## License
Qgis Forest Accessibility map py code © 2024 by Scotti Roberto is licensed under CC BY-SA 4.0 
<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gitlab.com/NuoroForestrySchool/qgis_proto_plugin.git">Qgis Forest Accessibility map py code</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://orcid.org/my-orcid?orcid=0000-0001-7394-4473">Scotti Roberto</a> is licensed under <a href="https://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-SA 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1" alt=""><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1" alt=""><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1" alt=""></a></p>

## Project status
